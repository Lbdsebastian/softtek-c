﻿using SofttekCharp.Entities;
using SofttekCSharp.Business.Interfaces;
using SofttekCSharp.Data.Models;
using SofttekCSharp.Data.Repositories.Interfaces;

namespace SofttekCSharp.Business
{
    public class MessagesBusiness : IMessagesBusiness
    {
        private readonly IUnitOfWork _unitOfWork;
        public MessagesBusiness(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Messages CreateMsg(int? config, Messages obj)
        {
            string modifiedString = "";
            switch (config)
            {
                case 1:
                    modifiedString = obj.Details.ToUpper();
                    break;
                case 2:
                    modifiedString = obj.Details.ToLower();
                    break;
                case 3:
                    modifiedString = Normalize.RemoveDiacritics(obj.Details);
                    break;
            }
            if (modifiedString != "")
            {
                obj.Details = modifiedString;
            }
            return _unitOfWork.Messages.SendMessage(obj);
        }

        public Messages EditMsg(EditMsgVM obj)
        {
            return _unitOfWork.Messages.EditMessage(obj);
        }


        public Messages GetDetails(int id)
        {
            Messages oMsg = _unitOfWork.Messages.Get(id);
            string filteredMsg = BadWordsFilter.Filter(BadWordsFilter.badWords, oMsg.Details);

            if (filteredMsg != oMsg.Details)
            {
                oMsg.Censored = true;
                oMsg.Details = filteredMsg;
            }
            return oMsg;
        }

    }
}
