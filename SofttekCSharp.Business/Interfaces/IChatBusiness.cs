﻿using SofttekCSharp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SofttekCSharp.Business.Interfaces
{
    public interface IChatBusiness
    {
        public List<Chat> GetAll();
        public Chat GetById(int id);
        public Chat StartChat(Chat obj);
        public List<Chat> GetUserChats(int id);
        public Chat GetChatMessages(int id);
    }
}
