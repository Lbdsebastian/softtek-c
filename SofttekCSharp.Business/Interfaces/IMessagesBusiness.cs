﻿using SofttekCharp.Entities;
using SofttekCSharp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SofttekCSharp.Business.Interfaces
{
    public interface IMessagesBusiness
    {
        public Messages GetDetails(int id);
        public Messages CreateMsg(int? config, Messages obj);
        public Messages EditMsg(EditMsgVM obj);
    }
}
