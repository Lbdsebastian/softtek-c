﻿using SofttekCharp.Entities;
using SofttekCSharp.Data.Models;
using SofttekCSharp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SofttekCSharp.Business.Interfaces
{
    public interface IUserBusiness
    {
        public List<Users> GetActiveUsers();
        public Users GetDetails(int id);
        public Users Register(Users obj);
        public string Authenticate(LoginVM obj);
    }
}
