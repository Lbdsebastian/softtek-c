﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using SofttekCharp.Entities;
using SofttekCSharp.Business.Interfaces;
using SofttekCSharp.Data.Models;
using SofttekCSharp.Data.Repositories.Interfaces;
using SofttekCSharp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SofttekCSharp.Business
{
    public class UsersBusiness : IUserBusiness
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _config;
        public UsersBusiness(IUnitOfWork unitOfWork, IConfiguration config)
        {
            _unitOfWork = unitOfWork;
            _config = config;
        }

        public string Authenticate(LoginVM obj)
        {
            string password = Encrypt.GetSHA256(obj.Password);
            obj.Password = password;

            var user = _unitOfWork.Users.GetActiveUsers()
                .Where(x => x.Username == obj.Username && x.Password == obj.Password)
                .FirstOrDefault();

            if (user != null)
            {
                var tokenKey = _config["TokenKey"];
                string JWT = JWTManager.IssueJWT(user.Username,user.Role.Name, tokenKey);
                return JWT;
            }
            return null;
        }

        public List<Users> GetActiveUsers()
        {
            return _unitOfWork.Users.GetActiveUsers();
        }

        public Users GetDetails(int id)
        {
            return _unitOfWork.Users.Get(id);
        }

        public Users Register(Users obj)
        {
            var user = _unitOfWork.Users.Get().Result.Where(x => x.Username == obj.Username).FirstOrDefault();
            
            if (user == null)
            {
                string password = Encrypt.GetSHA256(obj.Password);
                obj.Password = password;
                _unitOfWork.Users.Insert(obj);
                var newUser = _unitOfWork.Users.Get().Result.LastOrDefault();
                return newUser;
            }
            return null;
        }
    }
}
