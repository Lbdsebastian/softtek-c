﻿using SofttekCSharp.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SofttekCSharp.Business.Interfaces
{
    public class RolesBusiness : IRolesBusiness
    {
        private readonly IUnitOfWork _unitOfWork;
        public RolesBusiness(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
    }
}
