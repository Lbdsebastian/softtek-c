﻿using SofttekCharp.Entities;
using SofttekCSharp.Business.Interfaces;
using SofttekCSharp.Data.Models;
using SofttekCSharp.Data.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace SofttekCSharp.Business
{
    public class ChatBusiness : IChatBusiness
    {
        private readonly IUnitOfWork _unitOfWork;
        public ChatBusiness(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public List<Chat> GetAll()
        {
            var request = _unitOfWork.Chat.GetActiveChats();
            return request.ToList();
        }

        public Chat GetById(int id)
        {
            return _unitOfWork.Chat.Get(id);
        }

        public Chat GetChatMessages(int id)
        {
            Chat oChat = _unitOfWork.Chat.GetChatMessages(id);
            List<Messages> auxList = new List<Messages>();

            foreach (var item in oChat.Messages.ToList())
            {
                string filteredMsg = BadWordsFilter.Filter(BadWordsFilter.badWords, item.Details);
                if (filteredMsg != item.Details)
                {
                    item.Censored = true;
                }
                item.Details = filteredMsg;
                auxList.Add(item);
            }
            oChat.Messages = auxList;
            return oChat;
        }

        public List<Chat> GetUserChats(int id)
        {
            return _unitOfWork.Chat.GetUserChats(id);
        }

        public Chat StartChat(Chat obj)
        {
            _unitOfWork.Chat.Insert(obj);
            var currentChat = _unitOfWork.Chat.GetUserChats(obj.User_1).LastOrDefault();
            return currentChat;
        }
    }
}
