﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SofttekCharp.Entities
{
    public static class BadWordsFilter
    {
        public static List<string> badWords = new List<string>() { "estupido", "estupida", "bobo", "boba", "gordo", "gorda", "idiota", "imbecil", "tonto"};
        public static string Filter(List<string> badWords, string input)
        {
            const string CensoredText = "!$#@%";
            const string Pattern = @"\b({0})(s?)\b";
            const RegexOptions Options = RegexOptions.IgnoreCase;

            IEnumerable<Regex> badWordMatchers = badWords.
                Select(x => new Regex(string.Format(Pattern, x), Options));

            string output = badWordMatchers.
           Aggregate(input, (current, matcher) => matcher.Replace(current, CensoredText));

            return output;
        }
    }
}
