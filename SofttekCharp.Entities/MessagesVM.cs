﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SofttekCharp.Entities
{
   public class MessagesVM
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Details { get; set; }
        public string Modified { get; set; }
        public string Censored { get; set; }
        public string DateTime { get; set; }

    }
}
