﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SofttekCharp.Entities
{
    public class EditMsgVM
    {
        [Range(1, int.MaxValue, ErrorMessage = "Please insert a number greater than 0")]
        public int ChatId { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Please insert a number greater than 0")]
        public int UserId { get; set; }
        [Required]
        [MinLength(2, ErrorMessage = "Message cannot be shorter than 2 characters"),
            MaxLength(400, ErrorMessage = "Message cannot be longer than 400 characters.")]
        public string Modif { get; set; }
    }
}
