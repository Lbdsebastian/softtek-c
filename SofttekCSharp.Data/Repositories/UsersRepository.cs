﻿using Microsoft.EntityFrameworkCore;
using SofttekCSharp.Data.Models;
using SofttekCSharp.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SofttekCSharp.Data.Repositories
{
    public class UsersRepository : GenericRepository<Users>, IUsersRepository
    {
        public UsersRepository(SofttekCSharpContext context) : base(context)
        {
        }

        public List<Users> GetActiveUsers()
        {
            return _context.Users
                    .Include(u => u.Role)
                    .Where(u => u.State == true)
                    .ToList();
        }
    }
}
