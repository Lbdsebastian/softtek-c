﻿using SofttekCharp.Entities;
using SofttekCSharp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SofttekCSharp.Data.Repositories.Interfaces
{
    public interface IMessagesRepository : IGenericRepository<Messages>
    {
        public Messages SendMessage(Messages obj);
        public Messages EditMessage(EditMsgVM obj);
        
    }
}
