﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SofttekCSharp.Data.Repositories.Interfaces
{
    public interface IGenericRepository<T> where T : class
    {
        Task<IEnumerable<T>> Get();
        T Get(int id);
        void Insert(T entity);
        void Delete(T entity);
        void Update(T entity);
    }
}
