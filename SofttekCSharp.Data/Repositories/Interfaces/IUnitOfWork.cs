﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SofttekCSharp.Data.Repositories.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRolesRepository Roles { get; }
        IUsersRepository Users { get; }
        IMessagesRepository Messages { get; }
        IChatrepository Chat { get; }

        Task<int> Complete();
    }
}
