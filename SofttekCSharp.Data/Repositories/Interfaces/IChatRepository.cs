﻿using SofttekCSharp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SofttekCSharp.Data.Repositories.Interfaces
{
    public interface IChatrepository : IGenericRepository<Chat>
    {
        public List<Chat> GetActiveChats();
        public List<Chat> GetUserChats(int id);
        public Chat GetChatMessages(int id);
    }
}
