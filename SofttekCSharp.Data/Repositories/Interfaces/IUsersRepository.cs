﻿using SofttekCharp.Entities;
using SofttekCSharp.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SofttekCSharp.Data.Repositories.Interfaces
{
    public interface IUsersRepository : IGenericRepository<Users>
    {
        public List<Users> GetActiveUsers();
    }
}
