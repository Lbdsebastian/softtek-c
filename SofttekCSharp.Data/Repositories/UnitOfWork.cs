﻿using SofttekCSharp.Data.Models;
using SofttekCSharp.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SofttekCSharp.Data.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly SofttekCSharpContext _context;
        public UnitOfWork(SofttekCSharpContext context)
        {
            _context = context;
            Users = new UsersRepository(_context);
            Roles = new RolesRepository(_context);
            Messages = new MessagesRepository(_context);
            Chat = new ChatRepository(_context);
        }

        public IRolesRepository Roles { get; set; }

        public IUsersRepository Users { get; set; }

        public IMessagesRepository Messages { get; set; }

        public IChatrepository Chat { get; set; }

        public async Task<int> Complete()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
