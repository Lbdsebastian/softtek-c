﻿using Microsoft.EntityFrameworkCore;
using SofttekCharp.Entities;
using SofttekCSharp.Data.Models;
using SofttekCSharp.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SofttekCSharp.Data.Repositories
{
    public class MessagesRepository : GenericRepository<Messages>, IMessagesRepository
    {
        public MessagesRepository(SofttekCSharpContext context) : base(context)
        {
        }

        public Messages EditMessage(EditMsgVM obj)
        {
            var msg = _context.Messages
                    .Where(m => m.Chat_Id == obj.ChatId && m.User_Id == obj.UserId)
                    .ToList()
                    .OrderBy(m => m.Message_Id)
                    .LastOrDefault();
            if (msg != null && msg.User_Id == obj.UserId)
            {
                msg.Details = obj.Modif;
                msg.Modified = true;
                _context.SaveChanges();
                return msg;
            }
            return null;
        }

        public Messages SendMessage(Messages obj)
        {
            var chat = _context.Chat.Where(c => c.Chat_Id == obj.Chat_Id).FirstOrDefault();
            if (obj.User_Id == chat.User_1 | obj.User_Id == chat.User_2)
            {
                Insert(obj);
                return obj;
            }
            obj = null;
            return obj;
        }
    }
}
