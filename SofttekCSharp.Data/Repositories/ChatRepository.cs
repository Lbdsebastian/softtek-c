﻿using Microsoft.EntityFrameworkCore;
using SofttekCSharp.Data.Models;
using SofttekCSharp.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SofttekCSharp.Data.Repositories
{
    public class ChatRepository : GenericRepository<Chat>, IChatrepository
    {
        public ChatRepository(SofttekCSharpContext context) : base(context)
        {     
        }

        public List<Chat> GetActiveChats()
        {
            var request = _context.Chat
                            .Where(c => c.State == true)
                            .ToList();
            return request;
        }

        public Chat GetChatMessages(int id)
        {
            var request = _context.Chat
                        .Include(c => c.Messages)
                        .Where(c => c.Chat_Id == id).FirstOrDefault();
            return request;
        }

        public List<Chat> GetUserChats(int id)
        {
            var request = _context.Chat.Where(c => c.User_1 == id).ToList();
            return request;
        }


    }
}
