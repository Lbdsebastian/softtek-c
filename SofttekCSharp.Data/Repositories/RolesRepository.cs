﻿using SofttekCSharp.Data.Models;
using SofttekCSharp.Data.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SofttekCSharp.Data.Repositories
{
    public class RolesRepository : GenericRepository<Roles>, IRolesRepository
    {
        public RolesRepository(SofttekCSharpContext context) : base(context)
        {
        }
    }
}
