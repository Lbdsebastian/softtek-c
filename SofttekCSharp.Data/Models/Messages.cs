﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofttekCSharp.Data.Models
{
    public class Messages
    {
        public Messages()
        {
            Modified = false;
            State = true;
            Date_Time = DateTime.Now;
            Censored = false;
        }
        [Key]
        public int Message_Id { get; set; }

        [Required]
        [MinLength(2, ErrorMessage = "Message cannot be shorter than 2 characters"),
            MaxLength(400, ErrorMessage = "Message cannot be longer than 400 characters.")]
        public string Details { get; set; }
        public bool Modified { get; set; }
        public bool State { get; set; }
        public bool Censored { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime Date_Time { get; set; }

        //Navigation
        [Range(1, int.MaxValue, ErrorMessage = "Please insert a number greater than 0")]
        public int User_Id { get; set; }
        [ForeignKey("User_Id")]
        public Users User { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Please insert a number greater than 0")]
        public int Chat_Id { get; set; }
        [ForeignKey("Chat_Id")]
        public Chat Chat { get; set; }

    }
}
