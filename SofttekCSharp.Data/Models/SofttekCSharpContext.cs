﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SofttekCSharp.Data.Models
{
    public class SofttekCSharpContext : DbContext
    {
        public SofttekCSharpContext(DbContextOptions<SofttekCSharpContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder) 
        {
            #region Role Seeding
            modelBuilder.Entity<Roles>().HasData
                (new Roles
                {
                    Role_Id = 1,
                    Name = "Admin",
                    Description = "Grants full access"
                }, 
                new Roles
                {
                    Role_Id = 2,
                    Name = "User",
                    Description = "Grants limited access"
                }
                );
            #endregion
            #region UserSeed
            modelBuilder.Entity<Users>().HasData(
                new Users 
                { 
                    User_Id = 1,
                    Username = "testUser",
                    Password = "1234567888",
                    Role_Id = 2,
                    State = true
                },
                new Users
                {
                    User_Id = 2,
                    Username = "testUser2",
                    Password = "1234567888",
                    Role_Id = 2,
                    State = true
                },
                new Users
                {
                    User_Id = 3,
                    Username = "testUser3",
                    Password = "1234567888",
                    Role_Id = 2,
                    State = true
                },
                new Users
                {
                    User_Id = 4,
                    Username = "testUser4",
                    Password = "1234567888",
                    Role_Id = 2,
                    State = true
                },
                new Users
                {
                    User_Id = 5,
                    Username = "testUser5",
                    Password = "1234567888",
                    Role_Id = 2,
                    State = true
                }
                );
            #endregion
        }
        public DbSet<Roles> Roles { get; set; }
        public DbSet<Users> Users { get; set; }
        public DbSet<Messages> Messages { get; set; }
        public DbSet<Chat> Chat { get; set; }

    }
}
