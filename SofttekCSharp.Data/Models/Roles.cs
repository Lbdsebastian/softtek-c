﻿using System.ComponentModel.DataAnnotations;

namespace SofttekCSharp.Data.Models
{
    public class Roles
    {
        [Key]
        public int Role_Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
