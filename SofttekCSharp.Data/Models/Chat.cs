﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SofttekCSharp.Data.Models
{
    public class Chat
    {
        public Chat()
        {
            State = true;
        }
        [Key]
        public int Chat_Id { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please insert a number greater than 0")]
        public int User_1 { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please insert a number greater than 0")]
        public int User_2 { get; set; }
        public bool State { get; set; }

        //Navigation
        public ICollection<Messages> Messages { get; set; }
    }
}
