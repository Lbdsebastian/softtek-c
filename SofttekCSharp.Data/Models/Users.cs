﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SofttekCSharp.Data.Models
{
    public class Users
    {
        public Users()
        {
            State = true;
        }
        [Key]
        public int User_Id { get; set; }
        [Required]
        [MinLength(5, ErrorMessage = "Username lenght cannot be lower than 5 characters."),
            MaxLength(12, ErrorMessage = "Username lenght cannot be greater than 12 characters.")]
        public string Username { get; set; }
        [Required]
        [MinLength(8, ErrorMessage = "Password lenght cannot be lower than 8 characters")]
        public string Password { get; set; }
        public bool State { get; set; }

        //Navigation
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please insert a number greater than 0")]
        public int Role_Id { get; set; }
        [ForeignKey("Role_Id")]
        public Roles Role { get; set; }
        public ICollection<Messages> Messages { get; set; }

    }
}
