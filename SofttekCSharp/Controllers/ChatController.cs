﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SofttekCSharp.Business.Interfaces;
using SofttekCSharp.Data.Models;
using SofttekCSharp.Entities;
using System;

namespace SofttekCSharp.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize(Roles = Role.User)]
    public class ChatController : ControllerBase
    {
        private readonly IChatBusiness _chatBusiness;
        public ChatController(IChatBusiness chatBusiness)
        {
            _chatBusiness = chatBusiness;
        }

        /// <summary>
        /// devuelve las conversaciones activas de un usuario determinado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("active/{id}")]
        public IActionResult GetUserChats(int id)
        {
            var request = _chatBusiness.GetUserChats(id);
            return Ok(request);
        }

        /// <summary>
        /// permite iniciar una conversación con otro usuario
        /// </summary>
        /// <param name="oChat"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult StartChat(Chat oChat)
        {
            try
            {
                var request = _chatBusiness.StartChat(oChat);
                return Ok($"Chat created with ID: {request.Chat_Id}");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }

        /// <summary>
        /// permite obtener los mensajes que existen en una conversación
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        public IActionResult GetChatMessages(int id)
        {
            try
            {
                var request = _chatBusiness.GetChatMessages(id);
                return Ok(request);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
