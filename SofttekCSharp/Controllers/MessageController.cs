﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SofttekCharp.Entities;
using SofttekCSharp.Business.Interfaces;
using SofttekCSharp.Data.Models;
using SofttekCSharp.Entities;
using System;

namespace SofttekCSharp.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize(Roles = Role.User)]
    public class MessageController : ControllerBase
    {
        private readonly IMessagesBusiness _messageBusiness;
        public MessageController(IMessagesBusiness messageBusiness)
        {
            _messageBusiness = messageBusiness;
        }

        /// <summary>
        /// devuelve el detalle de un mensaje determinado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        public IActionResult GetDetails(int id)
        {
            try
            {
                var request = _messageBusiness.GetDetails(id);
                return Ok(request);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// permite enviar un mensaje a otro usuario mediante un chat existente, acepta diferentes parametros de configuración (1,2,3) para modificar el mensaje
        /// </summary>
        /// <param name="config"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("send")]
        public IActionResult SendMsg(int? config, [FromBody] Messages obj)
        {
            try
            {
               var request = _messageBusiness.CreateMsg(config, obj);
                if (request != null)
                {
                    return Ok($"Message sent: \n {request.Details}");
                }
                return BadRequest($"User with id '{obj.User_Id}' is not a member of chat with id '{obj.Chat_Id}'");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// permite modificar el ultimo mensaje enviado
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult EditMsg([FromBody] EditMsgVM obj)
        {
            try
            {
                var request = _messageBusiness.EditMsg(obj);
                if (request != null)
                {
                    return Ok(request);
                }
                return NotFound("Message not found");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
