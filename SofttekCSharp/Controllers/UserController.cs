﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SofttekCSharp.Business.Interfaces;
using SofttekCSharp.Data.Models;
using SofttekCSharp.Entities;
using System;


namespace SofttekCSharp.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize(Roles = Role.User)]
    public class UserController : ControllerBase
    {
        private readonly IUserBusiness _userBusiness;

        public UserController(IUserBusiness userBusiness)
        {
            _userBusiness = userBusiness;
        }

        /// <summary>
        /// devuelve la lista de usuario activos
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetActiveUsers()
        {
            try
            {
                var request = _userBusiness.GetActiveUsers();
                return Ok(request);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        /// <summary>
        /// devuelve el detalle de un usuario
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        public IActionResult GetUserDetails(int id)
        {
            try
            {
                var request = _userBusiness.GetDetails(id);
                return Ok(request);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Permite registrar un nuevo usuario
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("register")]
        public IActionResult Register(Users obj)
        {
            try
            {
                var request = _userBusiness.Register(obj);
                if (request != null)
                {
                    return Ok($"User {request.Username} created with id {request.User_Id}");
                }
                return BadRequest("Username already exist");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// permite autenticar un usuario previamente registrado
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("auth")]
        public IActionResult Authenticate(LoginVM obj)
        {
            try
            {
                var request = _userBusiness.Authenticate(obj);
                if (request != null)
                {
                    return Ok(request);
                }
                return BadRequest("User not found. Check username and password");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
